import Axios from 'axios';
import { timeout } from '../utils/constans';

const axiosInstance = Axios.create({
  baseURL: 'https://lb7u7svcm5.execute-api.ap-southeast-1.amazonaws.com/dev/',
  timeout
});

const errorHandler = error => {
  return Promise.reject({ ...error });
};

axiosInstance.interceptors.response.use(
  response => response,
  error => errorHandler(error),
);

export default axiosInstance;
