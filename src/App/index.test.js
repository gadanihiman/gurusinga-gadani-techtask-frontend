import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

describe('App component', () => {
  test('App should successfully rendered', () => {
    const component = renderer.create(
      <App  />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
