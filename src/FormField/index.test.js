import React from 'react';
import FormField from '.';
import renderer from 'react-test-renderer';

describe('FormField component', () => {
  test('FormField should successfully rendered', () => {
    const component = renderer.create(
      <FormField  />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
