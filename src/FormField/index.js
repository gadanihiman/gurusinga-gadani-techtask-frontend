import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  TextField,
  Button,
} from '@material-ui/core';
import moment from 'moment';
import { isEmpty, get } from 'lodash';
import request from '../api/globalConfig';
import Recipes from './components/Recipes';
import Ingredients from './components/Ingredients';

const useStyles = makeStyles(() => ({
  formControl: {
    marginTop: 10,
    marginBottom: 20,
    minWidth: 120,
    maxWidth: 300,
  },
  container: {
    marginTop: '45px',
  },
  textField: {
    width: 200,
  },
  textFieldWrapper: {
    marginTop: '30px',
    width: '100%',
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
}));

const today = new Date();

const ingredientsFilter = (ingredients, lunchDate = today) => {
  const ingredientsData = get(ingredients, 'data', []);
  return ingredientsData.filter(ingredient =>
    moment(lunchDate).isBefore(ingredient['use-by']) && ingredient
  );
}

const FormField = () => {
  const classes = useStyles();
  const [selectedIngredients, setSelectedIngredients] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    fetchIngredients();
  }, []);
  
  const fetchIngredients = async (lunchDate = today) => {
    try {
      const ingredients = await request('ingredients');
      if (ingredients) {
        const notExpiredIngredients = ingredientsFilter(ingredients, lunchDate)
        setIngredients(notExpiredIngredients);
      };
    } catch (error) {
      // TODO: display toast error
      console.log('error', error);
    }
  };

  const seeAvailableRecipes = async () => {
    try {
      const ingredientsQuery = selectedIngredients.join(',');
      const recipes = await request(`recipes?ingredients=${ingredientsQuery}`);
      if (recipes) {
        setRecipes(recipes.data);
      };
    } catch (error) {
      // TODO: display toast error
      console.log('error', error);
    }
  };

  const ingredientsList = ingredients.map(ingredient => ingredient.title);

  return (
    <div className={classes.container}>
      <div>
        <h3> What do you want to eat? </h3>
      </div>
      <div>
        <div className={classes.textFieldWrapper}>
          <TextField
            id="date"
            label="Lunch Date"
            type="date"
            defaultValue={moment(today).format('YYYY-MM-DD')}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={event => {
              setRecipes([]);
              setIngredients([]);
              setSelectedIngredients([]);
              fetchIngredients(event.target.value);
            }}
          />
        </div>
        <Ingredients
          ingredientsList={ingredientsList}
          classes={classes}
          ingredientController={{
            selectedIngredients, setSelectedIngredients
          }}
        />
        <div className={classes.formControl}>
          <Button
            variant="contained"
            color="primary"
            disabled={isEmpty(selectedIngredients)}
            onClick={() => seeAvailableRecipes()}
          >
            See what you can make!
          </Button>
        </div>
        <Recipes recipes={recipes} classes={classes} />
      </div>
    </div>
  );
};

export default FormField;
