import React from 'react';
import Ingredients from '.';
import renderer from 'react-test-renderer';

describe('Ingredients component', () => {
  const selectedIngredients = [];
  const setSelectedIngredients = () => {};
  const ingredientController = { selectedIngredients, setSelectedIngredients };
  test('Ingredients with empty ingredientsList props', () => {
    const component = renderer.create(
      <Ingredients 
        ingredientsList={[]}
        ingredientController={{}}
        classes={{}}
        theme={()=>{}}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  test('Ingredients show the ingredients dropdown if there are any ingredients props', () => {
    const component = renderer.create(
      <Ingredients 
        ingredientsList={['Butter', 'Cheese']}
        ingredientController={ingredientController}
        classes={{}}
        theme={()=>{}}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
