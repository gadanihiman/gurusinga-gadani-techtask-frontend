import React from 'react';
import { isEmpty } from 'lodash';
import { array, objectOf, any } from 'prop-types';
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Chip,
  Input,
} from '@material-ui/core';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Ingredients = ({
  classes,
  ingredientsList,
  ingredientController,
}) => {
  const { selectedIngredients, setSelectedIngredients } = ingredientController;
  return !isEmpty(ingredientsList) ?
    (
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-mutiple-chip-label">Ingredients</InputLabel>
        <Select
          labelId="demo-mutiple-chip-label"
          id="demo-mutiple-chip"
          multiple
          value={selectedIngredients}
          onChange={event => setSelectedIngredients(event.target.value)}
          input={<Input id="select-multiple-chip" />}
          renderValue={selected => (
            <div className={classes.chips}>
              {selected.map(value => (
                <Chip key={value} label={value} className={classes.chip} />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {ingredientsList.map(name => (
            <MenuItem key={name} value={name}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    ) : (
      <h3 className={classes.formControl}> No Ingredients available for this date </h3>
    )
};

Ingredients.propTypes = {
  ingredientsList: array,
  ingredientController: objectOf(any).isRequired,
  classes: objectOf(any).isRequired,
};

Ingredients.defaultProps = {
  ingredientsList: [],
}

export default Ingredients;

