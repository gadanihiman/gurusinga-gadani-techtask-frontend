import React from 'react';
import { isEmpty } from 'lodash';
import { array } from 'prop-types';

const Recipes = ({ recipes, classes }) => (
  <div className={classes.formControl}>
    {!isEmpty(recipes) &&
      <>
          <h3>This is our suggestions!</h3>
          {recipes.map((recipe, key) => (
            <div key={recipe.title+key}>
              <h4>{recipe.title}</h4>
              <ol>
              {!isEmpty(recipe.ingredients) &&
                recipe.ingredients.map((ingredient, key) => (
                  <li key={ingredient+key}>
                    {ingredient}
                  </li>
              ))}
              </ol>
            </div>
          ))}
      </>
    }
  </div>
);

Recipes.propTypes = {
  recipes: array,
};

Recipes.defaultProps = {
  recipes: [],
}

export default Recipes;

