import React from 'react';
import Recipes from '.';
import renderer from 'react-test-renderer';

describe('Recipes component', () => {
  const recipes = [{
    title: "Ham and Cheese Toastie",
    ingredients: ["Ham", "Cheese", "Bread", "Butter"],
  }];
  test('Ingredients with empty recipes props', () => {
    const component = renderer.create(
      <Recipes 
        recipes={[]}
        classes={{}}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  test('Recipes show the recipes component if there are any ingredients props', () => {
    const component = renderer.create(
      <Recipes 
        recipes={recipes}
        classes={{}}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
