# App for Finding Recipes with Your Ingredients!
<br />

### This App is only contain one simple single page application

### NOTES: I see the api for getting the Recipes has some problems, because when i'm trying to request with different recipes its always return the same recipes. But for frontend stuff is good to go!

## Available Scripts

In the project directory, you can run:

### `yarn start (running the app)`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test (running tests)`

Launches the test runner in the interactive watch mode.<br />

### `yarn run coverage`

Running the coverage for the testing with jest cover

### `yarn build (deployment)`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
